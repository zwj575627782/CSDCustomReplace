﻿using AutoReplace;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSDCustomReplace.Scripts;

namespace CSDCustomReplace
{
    public partial class UI_List_Group : UserControl
    {
        private UI_List_Item uIPanelSub = new UI_List_Item();

        private FlowLayoutPanel list = new FlowLayoutPanel();
        public UI_List_Group()
        {
            InitializeComponent();
            this.list.Visible = false;
            this.list.Size = new Size(groupBox.Width - 6, 0);
            this.list.MaximumSize = new Size(groupBox.Width - 6, 481);
            this.list.AutoSize = true;
            groupBox.Controls.Add(list);
            this.list.Location = new Point(3, groupBox.Height);
            this.list.Visible = true;
            this.list.ControlRemoved += (_obj, e) =>
            {
                if (list.Controls.Count == 0)
                {
                    this.comboBoxRoot.Enabled = true;
                    this.list.Height = 0;
                }
            };
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {

            uIPanelSub = new UI_List_Item(this.comboBoxRoot.SelectedIndex);

            this.list.Controls.Add(uIPanelSub);
            this.comboBoxRoot.Enabled = false;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void SetLabel(int i)
        {
            this.groupBox.Text = "第" + i + "项";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnAdd.Enabled = true;
            this.resPanel.Visible = true;
        }
        public S_UserData GetData()
        {
            S_UserData user=new S_UserData();
            user.id = this.comboBoxRoot.SelectedIndex;
            user.res_name =this.textBoxBtn.Text;
            //Console.WriteLine(user.res_name+"wwww");
            List<S_UserData> data_list = new List<S_UserData>();
      
            foreach (UI_List_Item item in this.list.Controls)
            {
                user.data.Add(item.GetData());
            }
            return user;

        }
    }
}
