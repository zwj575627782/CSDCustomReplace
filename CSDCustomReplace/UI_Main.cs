using CSDCustomReplace;
using CSDCustomReplace.Scripts;
using System.Threading;
using System.Windows.Forms;

namespace AutoReplace
{
    public partial class UI_Main : Form
    {
        UI_Get_Path window = new();

        public UI_Main()
        {

            InitializeComponent();
            window.StartPosition = FormStartPosition.CenterScreen;
            window.ShowDialog();
            label_path.Text = "当前工程目录：" + window.default_path;
            this.listContent.ControlRemoved += (_obj, e) =>
            {
                RefreshLayout();
            };

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //添加子项并编号
            UI_List_Group ui_list_group = new UI_List_Group();
            ui_list_group.SetLabel(listContent.Controls.Count + 1);
            this.listContent.Controls.Add(ui_list_group);

        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            this.listContent.Controls.Clear();
        }

        //移除子项后刷新主界面排版，重新编号
        public void RefreshLayout()
        {
            for (int i = 0; i < listContent.Controls.Count; i++)
            {
                ((UI_List_Group)listContent.Controls[i]).SetLabel(i + 1);
            }

        }

        private void btnRun_Click(object sender, EventArgs e)
        {

            btnClean.Enabled = false;
            btnLoadConfig.Enabled = false;
            btnAdd.Enabled = false;
            listContent.Enabled = false;
            btnRun.Enabled = false;
            btnPath.Enabled = false;

            //看看路径是不是空的,PS这里改反过 
            if (this.label_path.Text == "")
            {
                MessageBox.Show("路径不存在，请检查路径");
            }
            else if (CheckControls(this.listContent))
            {
                //开始运行
                SendToRun(this.listContent);

                MessageBox.Show("修改完成");
            }
            ReleaseUI();

        }

        public void ReleaseUI()
        {
            btnClean.Enabled = true;
            btnLoadConfig.Enabled = true;
            btnAdd.Enabled = true;
            listContent.Enabled = true;
            btnRun.Enabled = true;
            btnPath.Enabled = true;
        }

        //TODO:未完成
        public bool CheckControls(Control root)
        {
            Control.ControlCollection collection = root.Controls;
            string result = "";
            int i = 0;
            //获取每个ListGroup的修改值
            foreach (UI_List_Group control in collection)
            {
                i++;
                result += S_CheckList._Instance.Check(control.GetData(), i);
            }

            //检查结果为空等于没有错误
            if (result != "")
            {
                MessageBox.Show(result, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
                return true;
        }


        public void SendToRun(Control root)
        {
            //传入UI_List_Group
            Control.ControlCollection collection = root.Controls;
            //获取每个ListGroup的修改值
            foreach (UI_List_Group control in collection)
            {
                S_XmlLoad._Instance.XmlLoad(control.GetData());
            }
        }

        private void btnPath_Click(object sender, EventArgs e)
        {
            window.ShowDialog();
            label_path.Text = "当前工程目录：" + window.default_path;
        }

        private void btnLoadConfig_Click(object sender, EventArgs e)
        {
            S_UserData all_data = new S_UserData();
            foreach (UI_List_Group control in this.listContent.Controls)
            {
                all_data.data.Add(control.GetData());
            }
            S_Config._Instance.SaveConfig(all_data);

        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {

        }
    }
}