﻿namespace CSDCustomReplace
{
    partial class UI_Panel_res
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            textBoxMod = new TextBox();
            label_tips = new Label();
            textBoxOrg = new TextBox();
            label2 = new Label();
            SuspendLayout();
            // 
            // textBoxMod
            // 
            textBoxMod.Location = new Point(566, 14);
            textBoxMod.Margin = new Padding(2);
            textBoxMod.Name = "textBoxMod";
            textBoxMod.Size = new Size(186, 23);
            textBoxMod.TabIndex = 9;
            // 
            // label_tips
            // 
            label_tips.AutoSize = true;
            label_tips.Location = new Point(510, 16);
            label_tips.Margin = new Padding(2, 0, 2, 0);
            label_tips.Name = "label_tips";
            label_tips.Size = new Size(56, 17);
            label_tips.TabIndex = 8;
            label_tips.Text = "修改数值";
            // 
            // textBoxOrg
            // 
            textBoxOrg.Location = new Point(265, 14);
            textBoxOrg.Margin = new Padding(2);
            textBoxOrg.Name = "textBoxOrg";
            textBoxOrg.Size = new Size(182, 23);
            textBoxOrg.TabIndex = 7;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(209, 16);
            label2.Margin = new Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new Size(56, 17);
            label2.TabIndex = 6;
            label2.Text = "原始数值";
            // 
            // UI_Panel_res
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Silver;
            Controls.Add(textBoxMod);
            Controls.Add(label_tips);
            Controls.Add(textBoxOrg);
            Controls.Add(label2);
            Margin = new Padding(2);
            Name = "UI_Panel_res";
            Size = new Size(811, 50);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox textBoxMod;
        private Label label_tips;
        private TextBox textBoxOrg;
        private Label label2;
    }
}
