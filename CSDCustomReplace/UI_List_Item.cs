﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSDCustomReplace.Scripts;

namespace CSDCustomReplace
{

    public partial class UI_List_Item : UserControl
    {
        Dictionary<int, List<string>> items = new Dictionary<int, List<string>>();
        private UI_Panel_res uIPanel_ResReplace;
        public UI_List_Item(int index)
        {
            items.Add(0, new List<string>() { "资源替换", "字体大小", "字体颜色修改" });//按钮
            items.Add(1, new List<string>() { "资源替换" });//图片


            InitializeComponent();
            this.comboBoxSub.Items.Clear();
            this.comboBoxSub.Items.AddRange(items[index].ToArray());
            this.comboBoxSub.SelectedIndex = 0;
        }
        public UI_List_Item()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.Parent.Height -= this.Height;
            //if (this.Parent.Controls.Count <= 1)
            //{
            //    this.Parent.Height = 0;
            //}
            //触发子项的重新排版
            this.Parent.Controls.Remove(this);


        }

        private void comboBoxSub_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (uIPanel_ResReplace != null) { return; }
            //uIPanel_ResReplace = new UI_Panel_res();
            //uIPanel_ResReplace.Location = new Point(0, this.Height);
            //this.Controls.Add(uIPanel_ResReplace);
            switch (comboBoxSub.SelectedIndex)
            {
                case 0: label_tips.Text = "修改资源"; break;
                case 1: label_tips.Text = "修改字号为"; break;
                case 2: label_tips.Text = "修改颜色为"; break;

            }
        }
        public S_UserData GetData()
        {
            S_UserData data = new S_UserData();
            data.id=this.comboBoxSub.SelectedIndex;
            data.res_name = textBoxNew.Text;
            return data;    
        }
    }
}
