﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CSDCustomReplace.Scripts
{
    internal class S_CheckList:Instance<S_CheckList>
    {
        private string _text = string.Empty;

        public S_CheckList() {
        }

        public string Check(S_UserData user,int index)
        {
            string result = "";
            string _text = "第" + index + "项的";

            if (user.id == null)
            {
                result += _text + "选项不能为空\n";
            }

            if(user.res_name == "")
            {
                result += _text + "引用资源不能为空\n";
            }

            for (int i=0;i<user.data.Count;i++)
            {
                //如果选的是按钮和图片
                if(user.id == 0 | user.id == 1)
                {
                    switch (user.data[i].id)
                    {
                        case 0:
                            if (user.data[i].res_name == "")
                            {
                                result += _text + "修改资源不能为空\n";
                            }
                            break;
                        case 1:
                            if (!IsNumeric(user.data[i].res_name))
                            {
                                result += _text + "修改字号不能为空或只能为数字\n";
                            }
                            break;
                        case 2:
                            if (!IsValidColor(user.data[i].res_name))
                            {
                                result += _text + "修改色值不能为空或填写错误\n";
                            }
                            break;
                    }
                }
                
            }
            return result;

        }

        public static bool IsNumeric(string input)
        {
            if (input == "")
            {
                return false;
            }
            // 定义正则表达式
            string pattern = "^[0-9]+$";

            // 检查输入字符串是否符合正则表达式规则
            Regex regex = new Regex(pattern);
            return regex.IsMatch(input);
        }

        public static bool IsValidColor(string color)
        {
            // 定义正则表达式
            string pattern = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

            // 检查输入字符串是否符合正则表达式规则
            Regex regex = new Regex(pattern);
            return regex.IsMatch(color);
        }
    }
}
