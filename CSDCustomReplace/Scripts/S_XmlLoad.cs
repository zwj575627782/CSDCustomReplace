﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace CSDCustomReplace.Scripts
{
    

    internal class S_XmlLoad : Instance<S_XmlLoad>
    {
        List<string> res_path = new();

        public void GetPathList(string folder_path)
        {            
            try
            {
                FileSystemInfo[] fileSystems = new DirectoryInfo(folder_path).GetFileSystemInfos();
                res_path.Clear();
                foreach (var filesystem in fileSystems)
                {
                    if (File.Exists(filesystem.FullName))
                    {                        
                        Console.WriteLine("Load:" + filesystem.FullName);
                        res_path.Add(filesystem.FullName);
                    }
                }
            }
            catch {
                MessageBox.Show("路径不存在，请检查路径");
            }
                      
        }

        public S_XmlLoad()
        {
            
        }

        public void XmlLoad(S_UserData userData) {

            XmlDocument document = new XmlDocument();
            foreach (string path in res_path)
            {
                document.Load(path);
                GetAllXmlNode(document.SelectSingleNode("GameFile/Content/Content") as XmlElement, (node) =>
                {
                    if (node.Name == "AbstractNodeData")
                    {
                        //拿到node后开始走判断
                        for(int i = 0; i < userData.data.Count; i++)
                        {
                            switch (userData.id)
                            {
                                case 0: ChangeBtn(node as XmlElement, userData.data[i].id, userData.res_name, userData.data[i].res_name); break;
                                case 1: ChangeImg(node as XmlElement, userData.data[i].id, userData.res_name, userData.data[i].res_name); break;
                            }
                        }                       
                    }
                });
                document.Save(path);
            }                              
            
        }
        
        /// <summary>
        /// 一类修改项
        /// </summary>
        /// <param name="xe"></param>
        /// <param name="sub_index">二类修改项</param>
        /// <param name="res_rely">资源依赖</param>
        /// <param name="new_key">修改值</param>
        public void ChangeBtn(XmlElement xe, int sub_index, string res_rely, string new_key)
        {
            bool isfind = false;
            if (xe.GetAttribute(CSDAttribute._Instance.obj) == CSDObj._Instance.btn_obj)
            {
                foreach (XmlElement child in xe.ChildNodes)
                {
                    isfind = false;
                    //先找中filedata一次
                    if (child.Name.Contains(CSDNode._Instance.node_file))
                    {
                        if (child.GetAttribute(CSDAttribute._Instance.path).Contains(res_rely) && isfind == false)
                        {
                            isfind = true;
                            Console.WriteLine("Button get");
                            //再把这整个按钮发送去修改
                            //资源替换, 字体大小, 字体颜色修改
                            switch (sub_index)
                            {
                                case 0: ChangeBtnFileRes(xe, res_rely, new_key); break;
                                case 1: ChangeBtnFontSize(xe, new_key); break;
                                case 2: ChangeBtnFontColor(xe, new_key); break;
                            }
                        }
                    }
                }

            }

        }

        public void ChangeImg(XmlElement xe, int sub_index, string res_rely, string new_key)
        {
            if (xe.GetAttribute(CSDAttribute._Instance.obj) == CSDObj._Instance.img_obj)
            {
                foreach (XmlElement child in xe.ChildNodes)
                {
                    if (child.ChildNodes.Count > 0)
                    {
                        ChangeImg(child, sub_index, res_rely, new_key);
                    }
                    //资源替换
                    switch (sub_index)
                    {
                        case 0: Console.WriteLine("ChangeFileRes"); ChangeFileRes(child, res_rely, new_key); break;
                    }
                }

            }
        }

        public void ChangeBtnFontSize(XmlElement xe, string new_key)
        {
            foreach (XmlElement child in xe.ChildNodes)
            {
                if (child.GetAttribute(CSDAttribute._Instance.obj) == CSDObj._Instance.txt_obj)
                {
                    child.SetAttribute(CSDAttribute._Instance.font_size, new_key);
                }
                //如果有嵌套则继续遍历
                if (child.ChildNodes.Count > 0)
                {
                    ChangeBtnFontSize(child, new_key);
                }               
            }
                           
                
        }

        public void ChangeBtnFontColor(XmlElement xe , string new_key)
        {
            //做一个色值进制转换
            Color color = HexToRgb(new_key);
            foreach (XmlElement child in xe.ChildNodes)
            {
                if (child.GetAttribute(CSDAttribute._Instance.obj) == CSDObj._Instance.txt_obj)
                {
                    foreach (XmlElement sub_child in child.ChildNodes)
                    {
                        if (sub_child.Name == CSDNode._Instance.node_color)
                        {
                            sub_child.SetAttribute("R", color.R.ToString());
                            sub_child.SetAttribute("G", color.G.ToString());
                            sub_child.SetAttribute("B", color.B.ToString());
                        }
                    }
                }
                if (child.ChildNodes.Count > 0)
                {
                    ChangeBtnFontColor(child, new_key);
                }
            }
        }

        public void ChangeFileRes(XmlElement child, string res_rely, string new_key)
        {
            if (child.Name == CSDNode._Instance.node_file)
            {
                if (child.GetAttribute(CSDAttribute._Instance.path).Contains(res_rely))
                {
                    string path = child.GetAttribute(CSDAttribute._Instance.path);
                    child.SetAttribute(CSDAttribute._Instance.path, Regex.Replace(path, FileNameAttach(res_rely,".png"), FileNameAttach(new_key,".png")));
                }

            }
                
        }

        public void ChangeBtnFileRes(XmlElement xe, string res_rely, string new_key)
        {
            foreach(XmlElement child in xe.ChildNodes)
            {
                if (child.Name.Contains(CSDNode._Instance.node_file))
                {
                    if (child.GetAttribute(CSDAttribute._Instance.path).Contains(res_rely))
                    {
                        string path = child.GetAttribute(CSDAttribute._Instance.path);
                        child.SetAttribute(CSDAttribute._Instance.path, Regex.Replace(path, FileNameAttach(res_rely, ".png"), FileNameAttach(new_key, ".png")));
                    }

                }
            }          
        }

        public static Color HexToRgb(string hexColor)
        {
            hexColor = hexColor.Replace("#", "");
            int r = int.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            int g = int.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            int b = int.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return Color.FromArgb(r, g, b);
        }

        private static void GetAllXmlNode(XmlElement node, Action<XmlElement> action)
        {
            if(node is null) { return; }
            foreach (XmlElement child in node.ChildNodes)
            {
                action(child);
                if (child.ChildNodes.Count>0)
                {
                    GetAllXmlNode(child, action);
                }
            }
        }

        private string FileNameAttach(string name,string attach)
        {
            if (!name.Contains(attach))
            {
                name += attach;
            }
            return name;
        }
    }
}
