﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSDCustomReplace.Scripts
{
    internal class S_CSDClass
    {
    }
    //子Node名字
    public class CSDNode : Instance<CSDNode>
    {
        public string node_size = "Size";
        public string node_color = "CColor";
        public string node_file = "FileData";
        public string node_font = "FontResource";


        public string this[string type]
        {
            get
            {
                return this[type];
            }
        }

    }

    //每个节点的属性
    public class CSDAttribute : Instance<CSDAttribute>
    {
        public string font_size = "FontSize";
        public string text = "LabelText";
        public string custom_size = "IsCustomSize";
        public string obj = "ctype";
        public string path = "Path";

        public string this[string type]
        {
            get
            {
                return this[type];
            }
        }

    }

    //节点类型
    public class CSDObj : Instance<CSDObj>
    {
        public string btn_obj = "ButtonObjectData";
        public string img_obj = "ImageViewObjectData";
        public string txt_obj = "TextObjectData";

        public string this[string type]
        {
            get
            {
                return this[type];
            }
        }

    }
}
