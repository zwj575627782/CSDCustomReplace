﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSDCustomReplace.Scripts
{
    public class S_UserData:Instance<S_UserData>
    {
        public int id;
        public string res_name;
        public List<S_UserData> data = new List<S_UserData>();
    }
}
