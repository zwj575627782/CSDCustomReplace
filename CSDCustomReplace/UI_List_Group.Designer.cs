﻿namespace CSDCustomReplace
{
    partial class UI_List_Group
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox = new GroupBox();
            panelHead = new Panel();
            resPanel = new Panel();
            textBoxBtn = new TextBox();
            label1 = new Label();
            button1 = new Button();
            btnAdd = new Button();
            comboBoxRoot = new ComboBox();
            groupBox.SuspendLayout();
            panelHead.SuspendLayout();
            resPanel.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox
            // 
            groupBox.AutoSize = true;
            groupBox.Controls.Add(panelHead);
            groupBox.Dock = DockStyle.Fill;
            groupBox.Location = new Point(0, 0);
            groupBox.Name = "groupBox";
            groupBox.Size = new Size(1284, 126);
            groupBox.TabIndex = 5;
            groupBox.TabStop = false;
            groupBox.Text = "第n项";
            // 
            // panelHead
            // 
            panelHead.Controls.Add(resPanel);
            panelHead.Controls.Add(button1);
            panelHead.Controls.Add(btnAdd);
            panelHead.Controls.Add(comboBoxRoot);
            panelHead.Location = new Point(3, 25);
            panelHead.Name = "panelHead";
            panelHead.Size = new Size(1274, 72);
            panelHead.TabIndex = 0;
            // 
            // resPanel
            // 
            resPanel.BackColor = SystemColors.ControlLight;
            resPanel.Controls.Add(textBoxBtn);
            resPanel.Controls.Add(label1);
            resPanel.Location = new Point(464, 20);
            resPanel.Name = "resPanel";
            resPanel.Size = new Size(520, 38);
            resPanel.TabIndex = 3;
            resPanel.Visible = false;
            // 
            // textBoxBtn
            // 
            textBoxBtn.Location = new Point(156, 3);
            textBoxBtn.Name = "textBoxBtn";
            textBoxBtn.Size = new Size(354, 30);
            textBoxBtn.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(14, 7);
            label1.Name = "label1";
            label1.Size = new Size(133, 24);
            label1.TabIndex = 0;
            label1.Text = "引用资源(.png)";
            // 
            // button1
            // 
            button1.Location = new Point(1120, 21);
            button1.Name = "button1";
            button1.Size = new Size(112, 35);
            button1.TabIndex = 2;
            button1.Text = "移除";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // btnAdd
            // 
            btnAdd.Enabled = false;
            btnAdd.Location = new Point(306, 21);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(112, 35);
            btnAdd.TabIndex = 1;
            btnAdd.Text = "添加项";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // comboBoxRoot
            // 
            comboBoxRoot.FormattingEnabled = true;
            comboBoxRoot.Items.AddRange(new object[] { "按钮", "图片" });
            comboBoxRoot.Location = new Point(30, 21);
            comboBoxRoot.Name = "comboBoxRoot";
            comboBoxRoot.Size = new Size(271, 32);
            comboBoxRoot.TabIndex = 0;
            comboBoxRoot.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
            // 
            // UIListGroup
            // 
            AutoScaleDimensions = new SizeF(11F, 24F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            Controls.Add(groupBox);
            Name = "UIListGroup";
            Size = new Size(1284, 126);
            groupBox.ResumeLayout(false);
            panelHead.ResumeLayout(false);
            resPanel.ResumeLayout(false);
            resPanel.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private GroupBox groupBox;
        private Panel panelHead;
        private Button button1;
        private Button btnAdd;
        private ComboBox comboBoxRoot;
        private Panel resPanel;
        private TextBox textBoxBtn;
        private Label label1;
    }
}
