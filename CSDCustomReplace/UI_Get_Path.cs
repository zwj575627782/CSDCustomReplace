﻿using AutoReplace;
using CSDCustomReplace.Scripts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSDCustomReplace
{
    public partial class UI_Get_Path : Form
    {
        public string default_path;

        public UI_Get_Path()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() is DialogResult.OK)
            {
                this.path.Text = folderBrowserDialog1.SelectedPath;
                default_path = this.path.Text;
                //Properties.Settings.Default.Path();
            }

        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            LoadRes();
        }

        public bool LoadRes()
        {
            try
            {
                FileSystemInfo[] fileSystems = new DirectoryInfo(this.path.Text).GetFileSystemInfos();
                Thread thread = new Thread(() =>
                {
                    S_XmlLoad._Instance.GetPathList(this.path.Text);
                });
                thread.Start();

                this.Hide();
                return true;
            }
            catch
            {
                MessageBox.Show("找不到路径，请重新设置");
            }
            return false;
        }
    }
}
