﻿namespace AutoReplace
{
    partial class UI_Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI_Main));
            panel1 = new Panel();
            btnPath = new Button();
            label_path = new Label();
            btnLoadConfig = new Button();
            btnAdd = new Button();
            label1 = new Label();
            panel2 = new Panel();
            btnSaveConfig = new Button();
            btnClean = new Button();
            btnRun = new Button();
            listContent = new FlowLayoutPanel();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(btnPath);
            panel1.Controls.Add(label_path);
            panel1.Controls.Add(btnLoadConfig);
            panel1.Controls.Add(btnAdd);
            panel1.Controls.Add(label1);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(844, 100);
            panel1.TabIndex = 2;
            // 
            // btnPath
            // 
            btnPath.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnPath.Location = new Point(378, 39);
            btnPath.Name = "btnPath";
            btnPath.Size = new Size(130, 44);
            btnPath.TabIndex = 4;
            btnPath.Text = "修改目录";
            btnPath.UseVisualStyleBackColor = true;
            btnPath.Click += btnPath_Click;
            // 
            // label_path
            // 
            label_path.AutoSize = true;
            label_path.Location = new Point(24, 15);
            label_path.Name = "label_path";
            label_path.Size = new Size(92, 17);
            label_path.TabIndex = 3;
            label_path.Text = "当前工程路径：";
            // 
            // btnLoadConfig
            // 
            btnLoadConfig.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnLoadConfig.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnLoadConfig.Location = new Point(542, 39);
            btnLoadConfig.Margin = new Padding(2);
            btnLoadConfig.Name = "btnLoadConfig";
            btnLoadConfig.Size = new Size(130, 44);
            btnLoadConfig.TabIndex = 2;
            btnLoadConfig.Text = "读取配置";
            btnLoadConfig.UseVisualStyleBackColor = true;
            btnLoadConfig.Click += btnLoadConfig_Click;
            // 
            // btnAdd
            // 
            btnAdd.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnAdd.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnAdd.Location = new Point(687, 39);
            btnAdd.Margin = new Padding(2);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(130, 44);
            btnAdd.TabIndex = 1;
            btnAdd.Text = "添加";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(23, 59);
            label1.Name = "label1";
            label1.Size = new Size(298, 21);
            label1.TabIndex = 0;
            label1.Text = "请在右侧添加条目，在下方选择修改项：";
            // 
            // panel2
            // 
            panel2.Controls.Add(btnSaveConfig);
            panel2.Controls.Add(btnClean);
            panel2.Controls.Add(btnRun);
            panel2.Dock = DockStyle.Bottom;
            panel2.Location = new Point(0, 581);
            panel2.Name = "panel2";
            panel2.Size = new Size(844, 100);
            panel2.TabIndex = 3;
            // 
            // btnSaveConfig
            // 
            btnSaveConfig.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnSaveConfig.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSaveConfig.Location = new Point(542, 26);
            btnSaveConfig.Margin = new Padding(2);
            btnSaveConfig.Name = "btnSaveConfig";
            btnSaveConfig.Size = new Size(130, 44);
            btnSaveConfig.TabIndex = 4;
            btnSaveConfig.Text = "保存配置";
            btnSaveConfig.UseVisualStyleBackColor = true;
            btnSaveConfig.Click += btnSaveConfig_Click;
            // 
            // btnClean
            // 
            btnClean.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnClean.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnClean.Location = new Point(40, 26);
            btnClean.Margin = new Padding(2);
            btnClean.Name = "btnClean";
            btnClean.Size = new Size(130, 44);
            btnClean.TabIndex = 3;
            btnClean.Text = "清空内容";
            btnClean.UseVisualStyleBackColor = true;
            btnClean.Click += btnClean_Click;
            // 
            // btnRun
            // 
            btnRun.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnRun.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnRun.Location = new Point(687, 26);
            btnRun.Margin = new Padding(2);
            btnRun.Name = "btnRun";
            btnRun.Size = new Size(130, 44);
            btnRun.TabIndex = 2;
            btnRun.Text = "运行";
            btnRun.UseVisualStyleBackColor = true;
            btnRun.Click += btnRun_Click;
            // 
            // listContent
            // 
            listContent.AllowDrop = true;
            listContent.AutoScroll = true;
            listContent.AutoSize = true;
            listContent.BackColor = SystemColors.ControlLight;
            listContent.Dock = DockStyle.Fill;
            listContent.Location = new Point(0, 100);
            listContent.Name = "listContent";
            listContent.Size = new Size(844, 481);
            listContent.TabIndex = 5;
            listContent.TabStop = true;
            // 
            // UI_Main
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(844, 681);
            Controls.Add(listContent);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Margin = new Padding(2);
            Name = "UI_Main";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "CSD Custom Edit";
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Panel panel1;
        private Label label1;
        private Panel panel2;
        private Button btnAdd;
        private Button btnLoadConfig;
        private Button btnSaveConfig;
        private Button btnClean;
        private Button btnRun;
        private FlowLayoutPanel listContent;
        private Button btnPath;
        private Label label_path;
    }
}