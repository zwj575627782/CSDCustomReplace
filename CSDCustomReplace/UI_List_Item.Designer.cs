﻿namespace CSDCustomReplace
{
    partial class UI_List_Item
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            comboBoxSub = new ComboBox();
            button1 = new Button();
            textBoxNew = new TextBox();
            label_tips = new Label();
            SuspendLayout();
            // 
            // comboBoxSub
            // 
            comboBoxSub.FormattingEnabled = true;
            comboBoxSub.Items.AddRange(new object[] { "A", "B", "C", "D", "F" });
            comboBoxSub.Location = new Point(74, 12);
            comboBoxSub.Margin = new Padding(2);
            comboBoxSub.Name = "comboBoxSub";
            comboBoxSub.Size = new Size(226, 25);
            comboBoxSub.TabIndex = 0;
            comboBoxSub.SelectedIndexChanged += comboBoxSub_SelectedIndexChanged;
            // 
            // button1
            // 
            button1.Location = new Point(309, 12);
            button1.Margin = new Padding(2);
            button1.Name = "button1";
            button1.Size = new Size(81, 25);
            button1.TabIndex = 1;
            button1.Text = "移除子项";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // textBoxNew
            // 
            textBoxNew.Location = new Point(613, 14);
            textBoxNew.Name = "textBoxNew";
            textBoxNew.Size = new Size(171, 23);
            textBoxNew.TabIndex = 2;
            // 
            // label_tips
            // 
            label_tips.AutoSize = true;
            label_tips.Location = new Point(530, 17);
            label_tips.Name = "label_tips";
            label_tips.Size = new Size(44, 17);
            label_tips.TabIndex = 3;
            label_tips.Text = "修改项";
            // 
            // UI_List_Item
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Silver;
            Controls.Add(label_tips);
            Controls.Add(textBoxNew);
            Controls.Add(button1);
            Controls.Add(comboBoxSub);
            Margin = new Padding(2);
            MinimumSize = new Size(811, 0);
            Name = "UI_List_Item";
            Size = new Size(811, 50);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox comboBoxSub;
        private Button button1;
        private TextBox textBoxNew;
        private Label label_tips;
    }
}
