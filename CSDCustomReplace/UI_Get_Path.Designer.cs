﻿namespace CSDCustomReplace
{
    partial class UI_Get_Path
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI_Get_Path));
            label1 = new Label();
            path = new TextBox();
            btnBrowse = new Button();
            btnConfirm = new Button();
            folderBrowserDialog1 = new FolderBrowserDialog();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(24, 21);
            label1.Name = "label1";
            label1.Size = new Size(140, 17);
            label1.TabIndex = 0;
            label1.Text = "请输入或选择工程目录：";
            // 
            // path
            // 
            path.Location = new Point(24, 47);
            path.Name = "path";
            path.Size = new Size(217, 23);
            path.TabIndex = 1;
            // 
            // btnBrowse
            // 
            btnBrowse.Location = new Point(247, 47);
            btnBrowse.Name = "btnBrowse";
            btnBrowse.Size = new Size(75, 23);
            btnBrowse.TabIndex = 2;
            btnBrowse.Text = "选择";
            btnBrowse.UseVisualStyleBackColor = true;
            btnBrowse.Click += btnBrowse_Click;
            // 
            // btnConfirm
            // 
            btnConfirm.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            btnConfirm.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnConfirm.Location = new Point(129, 91);
            btnConfirm.Name = "btnConfirm";
            btnConfirm.Size = new Size(89, 33);
            btnConfirm.TabIndex = 3;
            btnConfirm.Text = "确认";
            btnConfirm.UseVisualStyleBackColor = true;
            btnConfirm.Click += btnConfirm_Click;
            // 
            // UI_Get_Path
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(344, 141);
            Controls.Add(btnConfirm);
            Controls.Add(btnBrowse);
            Controls.Add(path);
            Controls.Add(label1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MaximumSize = new Size(360, 180);
            MinimumSize = new Size(360, 180);
            Name = "UI_Get_Path";
            StartPosition = FormStartPosition.CenterParent;
            Text = "选择目录";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private TextBox path;
        private Button btnBrowse;
        private Button btnConfirm;
        private FolderBrowserDialog folderBrowserDialog1;
    }
}